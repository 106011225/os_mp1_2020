# NachOS assignments: System call


## Goal

Implement four I/O system calls in NachOS
- `OpenFileId Open(char *name)`
    - Open a file with the `name`, and return its corresponding `OpenFileId`.
    - Return `-1` if fail to open the file.
- `int Write(char *buffer, int size, OpenFileId id)`
    - Write `size` characters from the buffer into the file, and return the number of characters actually written to the file.
    - Return `-1` if fail to write the file.
- `int Read(char *buffer, int size, OpenFileId id)`
    - Read `size` characters from the file to the `buffer`, and return the number of characters actually read from the file.
    - Return `-1` if fail to read the file.
- `int Close(OpenFileId id)`
    - Close the file with `id`
    - Return `1` if successfully close the file. Otherwise return `-1`.

**Please refer to the [report](./MP1_report.pdf) for more details.**


## Appendix: What is NachOS?

> *NachOS* is instructional software for teaching undergraduate, and potentially graduate, level operating systems courses.  
> Website: https://homes.cs.washington.edu/~tom/nachos/
