/**************************************************************
 *
 * userprog/ksyscall.h
 *
 * Kernel interface for systemcalls 
 *
 * by Marcus Voelp  (c) Universitaet Karlsruhe
 *
 **************************************************************/

#ifndef __USERPROG_KSYSCALL_H__ 
#define __USERPROG_KSYSCALL_H__ 

#include "kernel.h"

#include "synchconsole.h"


void SysHalt()
{
  kernel->interrupt->Halt();
}

void SysPrintInt(int val)
{ 
  DEBUG(dbgTraCode, "In ksyscall.h:SysPrintInt, into synchConsoleOut->PutInt, " << kernel->stats->totalTicks);
  kernel->synchConsoleOut->PutInt(val);
  DEBUG(dbgTraCode, "In ksyscall.h:SysPrintInt, return from synchConsoleOut->PutInt, " << kernel->stats->totalTicks);
}

int SysAdd(int op1, int op2)
{
  return op1 + op2;
}

int SysCreate(char *filename)
{
	// return value
	// 1: success
	// 0: failed
	return kernel->fileSystem->Create(filename);
}


/* add by liuchinlin @ mp1 */
//When you finish the function "OpenAFile", you can remove the comment below.

OpenFileId SysOpenAFile(char *name)
{
    return kernel->fileSystem->OpenAFile(name);
}

int SysWriteFile(char *buf, int size, int id){
    return kernel->fileSystem->WriteFile(buf, size, id);
}

int SysReadFile(char *buf, int size, int id){
    return kernel->fileSystem->ReadFile(buf, size, id);
}

int SysCloseFile(int id){
    return kernel->fileSystem->CloseFile(id);
}

/* /add by liuchinlin @ mp1 */
#endif /* ! __USERPROG_KSYSCALL_H__ */
